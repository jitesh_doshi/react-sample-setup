# ReactJS Sample Setup #

This is a very small and simple starter ReactJS project. It has some very important build elements in place - such as webpack, babel, and hot/live reloading (the browser refreshes as soon as you save a change).

## What's in it? ##

* `build/index.html` - basic HTML entry point with an "app" container `DIV`, and a `SCRIPT` tag to include "bundle.js"
* `src/index.jsx` - basic JavaScript entry point to render `App` component inside the "app" `DIV` in the HTML page.
* `src/components/App.jsx` - Basic `App` component, that just says Hello.
* `package.json` - npm config file that specifies `build` and `start` script targets, ReactJS dependencies, babel/webpack devDependencies, and `index.js` JavaScript entry point.
* `.babelrc` - Babel config file that specifies `es2015` (ES6) and `react` presets for transpilation and hot reloading.
* `webpack.config.js` (note the JS extension, instead of JSON) - webpage config script that specifies target directory (`build`), target file (`bundle.js`), running of dev-server on "start", and hot-reloading, among other things.

## How to use it? ##

* Git clone this repo.
* Modify `name`, `version` and `description` in `package.json`.
* Edit `build/index.html` to your liking.
* Run `npm start` in the base directory (make sure you have [installed latest NPM and NodeJS](https://docs.npmjs.com/getting-started/installing-node)). Running this the first time will take a while, since it downloads all dependencies.
* Visit http://localhost:8080/ in the browser.
* Make changes to code and watch the browser reload them on save.
* Add more components in `src/components` directory.
* Modify `App` to make use of those new components.
* If you want to generate `build/bundle.js`, run `npm run build`.

Enjoy!
